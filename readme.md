Требования:

* A Linux/Unix based system

* Python 3.5 or greater

* nodejs/npm

При использовании pip, ставим последнюю вресию nodejs/npm. На убунте можно такой командой:

**sudo apt-get install npm nodejs-legacy**

The nodejs-legacy package installs the node executable and is currently required for npm to work on Debian/Ubuntu.

1. Ставим JupyterHub на сервер

**npm install -g configurable-http-proxy**

**python3 -m pip install jupyterhub**

2. Генерим рабочую конфигурацию

**jupyterhub --generate-config**

Запускаем jupyterhub командой 

**jupyterhub --ip <айпи-адрес> --port <порт> \[--ssl-key <ключ> --ssl-cert <сертификат>\]**//для https

3. Ставим [Докер](https://docs.docker.com/engine/install/) 

4. Качаем докер контейнер командой 

**docker pull ilnazman/bronze**

5. Запускаем контейнер

**docker run -p 8000:8000 -d --name bronze ilnazman/bronze jupyterhub**

6. Заходим на контейнер и добавляем юзеров с паролями командой

**adduser <имя_юзера>**

7. В файле БД jupyterhub.sqlite в таблице users добавляем тех же юзеров, что и в контейнере (также можно определенных юзеров отметить как админов, но разницу не заметил) (у меня сей файл лежал в директории пользователя)
пароли у всех юзеров 12345678
