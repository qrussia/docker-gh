FROM jupyterhub/singleuser

USER root

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update \
 && apt-get -yq dist-upgrade \
 && apt-get install -yq --no-install-recommends \
    gcc \
    python-dev \
    git-core \
 && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install matplotlib \
    jupyter \ 
    ipywidgets \
    pandas \
    numpy \
    scipy \
    qiskit \
    pyquil \
    dwave-system \
    pylatexenc \
    dwave-networkx

RUN rm -r ./work
RUN git+https://gitlab.com/qworld/bronze-qiskit.git
